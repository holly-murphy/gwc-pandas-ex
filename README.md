# gwc-pandas-ex

Prior to running tests, perform the following steps:  
1. install pandas
2. add the gooreadsbooks dataset to the same directory : https://www.kaggle.com/jealousleopard/goodreadsbooks
3. review the documentation on pandas :  https://pandas.pydata.org/pandas-docs/stable/ 

# Run tests:  
1. (from directory of lib-example.py) run the following command: ```python -m unittest lib-example.TestPandasOutput.TestMain```
