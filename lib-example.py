###
# demonstration of python libraries
# documentation: https://pandas.pydata.org/pandas-docs/stable/
# install: https://pypi.org/project/pandas/
# books sample dataset: https://www.kaggle.com/jealousleopard/goodreadsbooks
###
import unittest
import pandas 

def main():
    # read in data from csv
    data = pandas.read_csv('books.csv')

    # group data by publisher and get the count of books
    count_of_books_per_publisher = data.groupby('publisher').count()[["bookID"]]
    count_of_books_per_publisher.columns = [ 'Count of Books']

    print(f"ACE: {count_of_books_per_publisher.at['Ace', 'Count of Books']}")
    print(f"ADV Manga: {count_of_books_per_publisher.at['ADV Manga', 'Count of Books']}")
    print(f"ADV Manga: {count_of_books_per_publisher.loc['ADV Manga']}")

    with pandas.ExcelWriter("book-out.xlsx") as writer:
        count_of_books_per_publisher.to_excel(writer)

    return count_of_books_per_publisher

class TestPandasOutput(unittest.TestCase):
  def TestMain(self):
      result = main()

      self.assertEqual(result.at['Ace','Count of Books'],35)
